﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading;
using System.Diagnostics;

namespace yTechieSignalR
{
    public class DatasourceRecord : Hub
    {
        public void Hello()
        {
            Clients.All.hello("Hello from DatasourceHub @ " + DateTime.UtcNow.ToLongTimeString());
        }

        public const string GroupLabelPrefix = "Datasource_";

        public void Register(int datasourceId)
        {
            Groups.Add(Context.ConnectionId, GroupLabelPrefix + datasourceId);
        }

        public void Unregister(int datasourceId)
        {
            Groups.Remove(Context.ConnectionId, GroupLabelPrefix + datasourceId);
        }

        public static void NewDataRecord(DataRecord record)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<DatasourceRecord>();

            var groupName = GroupLabelPrefix + record.DatasourceId;
            var group = context.Clients.Group(groupName);
            if (group != null)
            {
                group.newRecord(record);
            }
        }

        public void StartTimer()
        {
            while (true)
            {
                Clients.All.showTime(DateTime.UtcNow.ToLongTimeString());
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
        }

        public void newVehicleRecordReceived(int vehicleId)
        {
            var message = "New Vehicle arrived with vehicleID=" + vehicleId + " notified by client=" + Context.ConnectionId;
            Clients.All.message(message);
            Console.WriteLine("Console: " + message);
            Debug.WriteLine("Debug: " + message);
        }

    }
}